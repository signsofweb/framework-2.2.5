/* FIX Missing Account Links in Mobile */
require(
['jquery','domReady!'
],
function ($) {
        $('.header.links').clone().appendTo('#store\\.links');
});

require(
    ['jquery'
    ],
    function ($) {
        $('.footer-quick-links').click(function () {
            $(this).toggleClass('active').parent().find('div.footer-top-content').stop().slideToggle('medium');
        });
});