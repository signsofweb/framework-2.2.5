<?php

namespace SoW\Blog\Block\Widget;

class Latest extends AbstractWidget
{
    protected $_post;
    protected $_coreRegistry = null;
    protected $_blogHelper;
    protected $_storeManager;
    protected $_template = 'widget/lastest.phtml';
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \SoW\Blog\Helper\Data $blogHelper,
        \SoW\Blog\Model\Post $post,
        array $data = []
    )
    {
        $this->_post = $post;
        $this->_blogHelper = $blogHelper;
        $this->_coreRegistry = $registry;
        $this->_storeManager = $context->getStoreManager();
        parent::__construct($context, $blogHelper);
    }


    public function getPostCollection()
    {
        $post = $this->_post;
        $postCollection = $post->getCollection()
            ->addFieldToFilter('status', 1)
            ->addStoreFilter($this->_storeManager->getStore()->getId())
            ->setOrder('created_at', 'DESC');
		
        $postCollection->getSelect()->limit($this->getOptions('post_count'));
        return $postCollection;
    }
    public function getDataSlider(){
        $options = array(
            'item_md' => ($this->getOptions('max_item') != '')? $this->getOptions('max_item') :4,
            'item_sm' => ($this->getOptions('medium_item') != '')? $this->getOptions('medium_item') :2,
            'item_xs' => ($this->getOptions('min_item') != '')? $this->getOptions('min_item') :1,
            'dots' => ($this->getOptions('show_dots') == 0)? false :true,
            'nav'=> ($this->getOptions('show_nav') == 0)? false :true,
            'loop'=> ($this->getOptions('loop') == 0)? false :true,
            'autoplay' => ($this->getOptions('autoplay') == 0)? false :true,
            'autoplaySpeed'=> ($this->getOptions('autoplay_speed') != '')? $this->getOptions('autoplay_speed') :3000,
        );
        return json_encode($options);
    }

}
