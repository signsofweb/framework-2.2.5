<?php

namespace SoW\Megamenu\Block\Adminhtml\Item;
use SoW\Megamenu\Model\ItemFactory;
class Tree extends \Magento\Backend\Block\Template{
    protected $_template = 'item/tree.phtml';

    protected $_itemFactory;
    protected $_urlBuilder;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        ItemFactory $itemFactory,
        array $data = [])
    {
        $this->_itemFactory = $itemFactory;
        $this->_urlBuilder = $context->getUrlBuilder();
        parent::__construct($context, $data);
    }

    public function drawMenu($parentId,$lv = 1){

        $itemEditUrl = $this->_urlBuilder->getUrl('megamenu/item/edit');
        if ($parentId == 0){
            $itemEditUrl = $this->_urlBuilder->getUrl('megamenu/item/editmenu');
        }
        $activeId = $this->getRequest()->getParam('item_id');
        $html = '';
        $collection = $this->_itemFactory->create()->getCollection();
        $collection->addFieldToFilter('parent_id',array('eq' => $parentId));
        $collection->addOrder('item_order','ASC');
        if ($collection->count() > 0 ){
            $html .= '<ul>';
            foreach ($collection as $item){
                $itemId = $item->getId();
                $itemClass = 'item lv-'.$lv;
                $itemAClass = '';
                if ($activeId == $itemId){
                    $itemClass .= ' active';
                    $itemAClass .= 'active';
                }
                $childrenHtml = $this->drawMenu($itemId,$lv +1);
                if ($childrenHtml){
                    $itemClass .= ' has_children';
                }

                $html .= '<li class="'.$itemClass.'"">';
                $html .= '<a href="'.$itemEditUrl.'item_id/'.$itemId.'" class="'.$itemAClass.'">';
                $html .= $item->getData('item_name');
                $html .= '</a>';
                if ($childrenHtml && $lv > 1){
                    $html.= '<div class="open"></div>';
                }
                $html .= $childrenHtml;
                $html .= '</li>';
            }
            $html .= '</ul>';
        }
        return $html;
    }

}