<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace SoW\Slideshow\Ui\Component\Listing\Column\Slider;

use Magento\Framework\Data\OptionSourceInterface;


class StyleType implements OptionSourceInterface
{

    public function toOptionArray()
    {
        $arr = [];
        $arr[]=[
            'value' => 1,
            'label' => 'Center'
        ];
        $arr[]=[
            'value' => 2,
            'label' => 'Right'
        ];
        $arr[]=[
            'value' => 3,
            'label' => 'Left'
        ];
        return $arr;
    }

}
