<?php
namespace SoW\Slideshow\Model;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Config {


    const XML_PATH_ENABLED ='slideshow/slider/status';

    const XML_PATH_SLIDER_DOTS ='slideshow/slider/dots';

    const XML_PATH_SLIDER_NAV ='slideshow/slider/nav';

    const XML_PATH_SLIDER_LOOP ='slideshow/slider/loop';

    const XML_PATH_SLIDER_AUTOPLAY ='slideshow/slider/autoplay';

    const XML_PATH_SLIDER_AUTOPLAYSPEED ='slideshow/slider/autoplayspeed';


    private $scopeConfigInterface;

    /**
     * @param ScopeConfigInterface ScopeConfigInterface
     */
    public function __construct(
        ScopeConfigInterface $scopeConfigInterface
    ) {
        $this->scopeConfigInterface = $scopeConfigInterface;
    }

    /**
     * Check if slider is enabled
     *
     * @return bool
     */
    public function getStatus(){
        if($this->scopeConfigInterface->getValue(self::XML_PATH_ENABLED,ScopeInterface::SCOPE_STORE) ==1){
            return true;
        }
        return false;
    }

    public function getDots(){
        return $this->scopeConfigInterface->getValue(self::XML_PATH_SLIDER_DOTS,ScopeInterface::SCOPE_STORE);
    }

    public function getNav(){
        return $this->scopeConfigInterface->getValue(self::XML_PATH_SLIDER_NAV,ScopeInterface::SCOPE_STORE);
    }

    public function getLoop(){
        return $this->scopeConfigInterface->getValue(self::XML_PATH_SLIDER_LOOP,ScopeInterface::SCOPE_STORE);
    }

    public function getAutoPlay(){
        return $this->scopeConfigInterface->getValue(self::XML_PATH_SLIDER_AUTOPLAY,ScopeInterface::SCOPE_STORE);
    }

    public function getAutoPlaySpeed(){
        return $this->scopeConfigInterface->getValue(self::XML_PATH_SLIDER_AUTOPLAYSPEED,ScopeInterface::SCOPE_STORE);
    }




}