<?php
namespace SoW\Slideshow\Block\Slider;
use Magento\Framework\View\Element\Template;
use SoW\Slideshow\Model\ResourceModel\Slider\Collection;
use SoW\Slideshow\Model\Config;

class Slidershow extends \Magento\Framework\View\Element\Template{
    protected  $_sliderCollection;

    protected $config;

    protected $_storeManager;
    public function __construct(
        Collection $sliderCollection,
        Template\Context $context,
        Config $config,
        array $data = []
    )
    {
        $this->sliderCollection = $sliderCollection;
        $this->_storeManager = $context->getStoreManager();
        $this->config = $config;
        parent::__construct($context, $data);
    }
    public function getSliderCollection(){
        $storeId = $this->_storeManager->getstore()->getId();

        $collection = $this->sliderCollection
            ->addFieldToFilter('slider_status',['neq' => '0'])
        ->addFieldToFilter(['store_ids','store_ids'],[['finset' => $storeId],['eq'=> 0]])->addOrder('slider_order','ASC');
        return $collection;

    }
    public function getDataSlider(){
        $options = array(
            'item_md' => 1,
            'item_sm' => 1,
            'item_xs' => 1,
            'dots' => ($this->config->getDots() == 0)? false :true,
            'nav'=> ($this->config->getNav() == 0)? false :true,
            'loop'=> ($this->config->getLoop() == 0)? false :true,
            'autoplay' => ($this->config->getAutoPlay() == 0)? false :true,
            'autoplaySpeed'=> ($this->config->getAutoPlaySpeed())? $this->config->getAutoPlaySpeed() :3000,
        );
        return json_encode($options);
    }
}