<?php
namespace SoW\Base\Block\Product;

class Popup extends \Magento\Catalog\Block\Product\AbstractProduct {
    /*
  * @var $_product
  */
    protected $_product;

    /*
     * @var $_subtotal
     */
    protected $_subtotal;

    /*
     * @var $_shippingPrice
     */
    protected $_shippingPrice;

    /*
     * @var $_coreRegistry
     */
    protected $_registry;

    protected $_helperPrice;

    /*
     * contructor
     */

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Pricing\Helper\Data $helperPrice,
        array $data = [])
    {
        $this->_helperPrice = $helperPrice;
        $this->_registry = $context->getRegistry();
        parent::__construct($context, $data);
    }

    /*
     * get product
     * @return \Magento\Catalog\Model\Product
     */

    public function getProduct(){
        if($this->_registry->registry('product')){
            $this->_product = $this->_registry->registry('product');
        }
        return $this->_product;
    }

    /*
     * set product
     * @return Ecentura\Infinitescroll\Block\Product\Popup
     */
    public function setProduct($product){
        $this->_product = $product;
        return $this;
    }

    /*
     * get product image
     */
    public function getProductImage($size){
        return $this->getImage($this->getProduct(),$size);
    }

    /*
     * setSubtotal
     */
    public function setSubtotal($subtotal){
        $this->_subtotal = $subtotal;
        return $this;
    }
    public function getProductName(){
        return $this->getProduct()->getName();
    }
    public function getPrice(){
        $finalPrice = $this->getProduct()->getFinalPrice();
        return $this->_helperPrice->currency($finalPrice, true, false);
    }

    /*
     * getsubtotal
     */
    public function getSubtotal(){
        return $this->_subtotal;
    }

    /*
     * set shipping price
     */
    public function setShippingPrice($price){
        $this->_shippingPrice = $price;
        return $this;
    }

}