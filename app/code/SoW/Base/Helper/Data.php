<?php
namespace SoW\Base\Helper;
use Magento\Framework\App\Helper\Context;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_storeManager;
    public function __construct(
        Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    public function getRefreshCartUrl(){
        $url = $this->_storeManager
            ->getStore()
            ->getUrl('base/quickview/updatecart');
        return $url;
    }

}