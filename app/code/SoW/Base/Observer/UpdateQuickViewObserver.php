<?php

namespace SoW\Base\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\View\Page\Config;


class UpdateQuickViewObserver implements ObserverInterface {
    protected $_request;
    protected $config;
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        Config $config
    )
    {
        $this->config = $config;
        $this->_request = $request;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $layout = $observer->getData('layout');
        $isQuickView = $this->_request->getParam('is_quickview', false);
        if (!$isQuickView) {
            return $this;
        }
        $isCustomize = $this->_request->getParam('is_customize', false);
        if ($isCustomize){
            $layout->getUpdate()->addHandle('base_customize_update');
        }
        $layout->getUpdate()->addHandle('base_quickview_update');
        $this->config->addBodyClass("sow_quick_view");
        return $this;
    }
}
