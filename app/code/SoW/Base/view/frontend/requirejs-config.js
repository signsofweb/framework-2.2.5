var config = {
	"map": {
		"*": {
			"sowowlcarousel": "SoW_Base/js/sow_owl_carousel",
			"magnificPopup": "SoW_Base/js/jquery.magnific-popup.min",
			"easytab": "SoW_Base/js/sow_easy_tab",
            "sowaddtocart" : "SoW_Base/js/addtocart",
            "quickview" : "SoW_Base/js/quickview"
		}
	},
	"paths": {
        "sowowlcarousel": "SoW_Base/js/sow_owl_carousel",
        "magnificPopup": "SoW_Base/js/jquery.magnific-popup.min",
        "easytab": "SoW_Base/js/sow_easy_tab",
        "quickview": "SoW_Base/js/quickview"
	},
    "shim": {
		"SoW_Base/js/owl.carousel.min": ["jquery"],
        "SoW_Base/js/sow_easy_tab":  ["jquery"],
        "SoW_Base/js/quickview":  ["jquery"],
	}
};
