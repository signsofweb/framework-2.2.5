define([
    'jquery',
    'magnificPopup',
    'Magento_Customer/js/customer-data',
    'jquery/ui',
    'Magento_Catalog/js/catalog-add-to-cart'
], function($, magnificPopup, customerData){
    var timer = null;
    $('#ajax_alert').mouseover(function() {
        clearTimeout(timer);
    }).mouseout(function() {
        timer = setTimeout(function () {
            $('#ajax_alert').removeClass('processing');
            $('#ajax_alert .ajax_res').html('');
            $('#ajax_alert').removeClass('loaded');
        },4000);
    });
    $('#ajax_alert .action.close').click(function () {
        $('#ajax_alert').removeClass('processing');
        $('#ajax_alert .ajax_res').html('');
        $('#ajax_alert').removeClass('loaded');
    });
    $.widget('mage.sowaddtocart', $.mage.catalogAddToCart, {

        ajaxSubmit: function (form) {
            var self = this;
            var dataUrl = form.attr('data-url');
            $(self.options.minicartSelector).trigger('contentLoading');
            self.disableAddToCartButton(form);

            $.ajax({
                url: form.attr('action'),
                data: form.serialize(),
                type: 'post',
                dataType: 'json',

                /** @inheritdoc */
                beforeSend: function () {
                    $('#ajax_alert').addClass('processing');
                    $('#ajax_alert .ajax_res').html('');
                    $('#ajax_alert').removeClass('loaded');
                    if (timer){
                            clearTimeout(timer);
                            timer = null
                    }
                    if (self.isLoaderEnabled()) {
                        $('body').trigger(self.options.processStart);
                    }
                },

                /** @inheritdoc */
                success: function (res) {
                    var eventData, parameters;
                    $(document).trigger('ajax:addToCart', {
                        'sku': form.data().productSku,
                        'form': form,
                        'response': res
                    });

                    if (self.isLoaderEnabled()) {
                        $('body').trigger(self.options.processStop);
                    }

                    if (res.backUrl) {
                        eventData = {
                            'form': form,
                            'redirectParameters': []
                        };
                        // trigger global event, so other modules will be able add parameters to redirect url
                        $('body').trigger('catalogCategoryAddToCartRedirect', eventData);

                        if (eventData.redirectParameters.length > 0) {
                            parameters = res.backUrl.split('#');
                            parameters.push(eventData.redirectParameters.join('&'));
                            res.backUrl = parameters.join('#');
                        }
                        self._Rewind();

                        $.magnificPopup.open({
                            items: {
                                src: dataUrl+'?is_quickview=[true]&is_customize=[true]'
                            },
                            type: 'iframe',
                            removalDelay: 300,
                            mainClass: 'sow_quick_view',
                            closeOnBgClick: false,
                            preloader: true,
                            tLoading: '',
                            callbacks: {
                                beforeClose: function() {
                                    customerData.reload('sections', true);
                                    $(self.options.productStatusSelector)
                                        .removeClass('available')
                                        .addClass('unavailable');
                                    self.enableAddToCartButton(form);
                                }
                            }
                        });

                        return;
                    }

                    if (res.messages) {
                        $(self.options.messagesSelector).html(res.messages);
                    }

                    if (res.minicart) {
                        $(self.options.minicartSelector).replaceWith(res.minicart);

                        $(self.options.minicartSelector).trigger('contentUpdated');
                    }

                    if (res.product && res.product.statusText) {
                        $(self.options.productStatusSelector)
                            .removeClass('available')
                            .addClass('unavailable')
                            .find('span')
                            .html(res.product.statusText);
                    }
                    self.enableAddToCartButton(form);
                    if (!res.backUrl){
                            self._RenderAlert(res.message);
                            timer = setTimeout(function () {
                                self._Rewind();
                            },4000);
                    }

                },
            });

        },
        _Rewind: function () {
            $('#ajax_alert').removeClass('processing');
            $('#ajax_alert .ajax_res').html('');
            $('#ajax_alert').removeClass('loaded');

        },
        _RenderAlert: function (mesa) {
            $('#ajax_alert .ajax_res').html(mesa);
            $('#ajax_alert').addClass('loaded');
        }

    });
    return $.mage.sowaddtocart;
});