define([
    'jquery',
    'magnificPopup',
    'Magento_Customer/js/customer-data'
], function ($, magnificPopup,customerData) {
    'use strict';
    $.widget('mage.quickview', {
        _init: function(){
            this._EventListener();
        },
        _EventListener: function () {
            var $widget = this;
            var el = this.element;
            el.click(function () {
                var href = el.data('href');
                $.magnificPopup.open({
                    items: {
                        src: href+'?is_quickview=[true]&is_customize=[true]'
                    },
                    type: 'iframe',
                    removalDelay: 300,
                    mainClass: 'sow_quick_view',
                    closeOnBgClick: false,
                    preloader: true,
                    tLoading: '',
                    callbacks: {
                        beforeClose: function() {
                            customerData.reload('sections', true);
                        }
                    }
                });
            });
        }

    });

    return $.mage.quickview;
});
