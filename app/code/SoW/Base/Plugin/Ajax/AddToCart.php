<?php
namespace SoW\Base\Plugin\Ajax;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Backend\App\Action\Context;
class AddToCart{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var $response
     */
    protected $response;

    /**
     * @var $cart
     */
    protected $cart;

    /**
     * @var $objectManage
     */
    protected $objectManage;

    /**
     * @var $messageManager
     */
    protected $messageManager;
    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $resultRawFactory;

    /*
     * @var pageFactory
     */
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        CustomerCart $cart,
        \Magento\Framework\App\Action\Context $context,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\View\Result\Page $page
    )
    {
        $this->resultRawFactory = $resultRawFactory;
        $this->resultPageFactory = $page;
        $this->scopeConfig = $scopeConfig;
        $this->cart = $cart;
        $this->messageManager = $context->getMessageManager();
        $this->productRepository = $productRepository;
        $this->objectManage = $context->getObjectManager();
    }


    public function afterExecute(\Magento\Checkout\Controller\Cart\Add $controller, $page){
        if($this->isAjax($controller))
        {
            if(empty($this->messageManager->getMessages()->getErrors())){
                $backurl = $controller->getRequest()->getParam('return_url');
                if($backurl){
                    $result['backUrl'] = $backurl;
                }
                $productId = $controller->getRequest()->getParam('product');
                $storeId = $this->objectManage->get('Magento\Store\Model\StoreManagerInterface')->getStore()->getId();
                $product = $this->productRepository->getById($productId, false, $storeId);
                $super_attribute = $controller->getRequest()->getParam('super_attribute');


                if($product->getTypeId()=='configurable'){
                    $isComeBack = true;
                    if ($super_attribute){
                        if (!in_array('',$super_attribute)){
                                $isComeBack = false;
                        }
                    }
                    if ($isComeBack){
                        $result['backUrl'] = $product->getUrlModel()->getUrl($product);
                        return $this->prepareResponse($result);
                    }
                }
                $result['status'] = 'success';
                $pageFactory = $this->objectManage->get('Magento\Framework\View\Result\PageFactory')->create()->getLayout();
                $result['message'] = $pageFactory->createBlock('SoW\Base\Block\Product\Popup')
                    ->setSubtotal($this->cart->getQuote()->getGrandTotal())
                    ->setProduct($product)
                    ->setTemplate('SoW_Base::product/popup.phtml')
                    ->toHtml();
                $response = $this->prepareResponse($result);
                return $response;
            }
        }
        return $page;
    }
    /**
     * @param $controller
     *
     * @return bool
     */
    protected function isAjax($controller)
    {
        $isAjax = $controller->getRequest()->isAjax();
        return $isAjax;
    }
    protected function prepareResponse(array $data)
    {
        $response = $this->resultRawFactory->create();
        $response->setHeader('Content-type', 'text/plain');
        $response->setContents(json_encode($data));
        return $response;
    }
}